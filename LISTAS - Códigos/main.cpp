#include <iostream>
#include <string.h>
#include <cstdlib> /*BIBLIOTECA PARA O MALLOC FUNCIONAR*/

struct elemento { /*CRIA N� E O DIVIDE EM DUAS PARTES: VALOR (INFO) E PONTEIRO*/
	int info;
	struct elemento *prox; /* * = ponteiro */
};
typedef struct elemento Elemento; /*D� NOME P/ SE REFERIR A ESSE STRUCT COMO 'Elemento'*/

//PROT�TIPOS
Elemento* lst_cria();
Elemento* lst_insere(Elemento* lst, int v);
void lst_imprime(Elemento* lst);
int lst_vazia(Elemento* lst);
Elemento* lst_buscar(Elemento* lst, int valor);
void libera_lista (Elemento* lst);

//MAIN
int main(int argc, char** argv) {
	Elemento* lst;
	lst = lst_cria();
	lst = lst_insere(lst, 25);
	lst = lst_insere(lst, 26);
	lst_imprime(lst); /*N�o retorna valor, por isso n�o precisa ser associado a nenhuma vari�vel*/
	lst_buscar(lst, 25);
	
	libera_lista (lst);
	
	/*Ap�s liberar a Lista, � preciso cria-la toda de novo se n�o d� erro (loop infinito)*/
	lst=lst_cria();
	lst = lst_insere (lst, 7);
	lst = lst_insere (lst, 8);
	lst = lst_insere (lst, 9);
	lst_imprime(lst);
	return 0;
}

//M�DULOS E FUN��ES
Elemento* lst_cria() { /*CRIA LISTA*/
	return NULL;
}

Elemento* lst_insere(Elemento* lst, int v) {
	Elemento* novo = (Elemento*) malloc (sizeof(Elemento));
	novo -> info = v;
	novo -> prox = lst;
	return novo;
}

void lst_imprime(Elemento* lst) {
	Elemento* p;
	for (p = lst; p!= NULL; p = p->prox) {
		printf("Numero: %d\n", p->info);
	}
}

int lst_vazia (Elemento* lst) {
	if (lst == NULL) {
		return 1;
	} else {
		return 0;
	}
}

Elemento* lst_buscar (Elemento* lst, int valor) {
	Elemento* p;
	for (p=lst; p!=NULL; p = p->prox) {
		if (p->info == valor) {
			return p;
		}
	}
	return NULL;
}

Elemento* lst_retira (Elemento* lst, int valor) {
	Elemento* ant = NULL;
	Elemento* p;
	
	for (p=lst; p != NULL && p->info != valor; p=p->prox) {
		ant = p;
		
		if (p == NULL) {
			return lst;
		}
		if (ant == NULL) {
			lst = p->prox;
		} else {
			ant -> prox = p->prox;
		}	
	}
	free(p);
	return lst;
}

void libera_lista (Elemento* lst) {
	Elemento *p, *t;
	
	for(p=lst; p!=NULL; p=t) {
		t = p->prox;
		free(p);
	}
}
