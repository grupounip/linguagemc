#ifndef LISTA_H
#include "Lista.h"
#include <cstdlib>
#endif
#include "CadastrarRecurso.h"

template <class List>
void Lista<List>::redefinir(){
	//redefine o tamanho da lista para poder armazenar mais dados
	//ao final do m�todo teremos uma lista que ter� o dobro do tamanho
	
	//criando uma lista tempor�ria:
	List *temp = new List[listlength * 2];
	for(int i = 0 ; i < listlength ; i++){
		temp[i] = data[i];
	}
	data = temp;
	listlength *= 2;
		
}

template <class List>
bool Lista<List>::precisaRedefinir(){
	//retorna se � poss�vel acomodar um novo objeto 
	return listlength == tamanhoList;
}

template <class List>
void Lista<List>::add(List item){
	//adiciona o item no topo da lista
	//checar se tem espa�o
	if (precisaRedefinir()){
		redefinir();
	}
	
	data[tamanhoList] = item;
	tamanhoList ++;
}

template <class List>
void Lista<List>::add(int index, List item){
	//adiciona o item em um index indicado
	if (precisaRedefinir()){
		redefinir();
	}
	
	for(int i = tamanholist ; i >= index ; i--){
		data[tamanhoList+1] = data[tamanhoList];
	}
	
	data[index] = item;
	tamanhoList++;
}

template<class List>
List* Lista<List>::get(int index){
	if (index >= 0 && index <= tamanhoList){
		return &data[index];
	}
	return NULL;
}

template<class List>
void Lista<List>::set(int index, List item){
	if (index >= 0 && index <= tamanhoList){
		data[index] = item;
	}
}

template<class List>
int Lista<List>::indexof(List item){
	for(int i = 0 ; i <= tamanhoList ; i++){
		if (item == data[i]){
			return i;
		}
	}
	
	//n�o encontrou
	return -1;
}

template<class List>
int Lista<List>::lastindexof(List item){
	for (int i = tamanhoList ; i>= 0 ; i--){
		if (item == data[i]){
			return i;
		}
		
		//n�o encontrou:
		return -1;	
	}
}

template <class List>
bool Lista<List>::contem(List item){
	return (indexof(item) > -1);
}

template <class List>
int Lista<List>::tamanho(){
	return tamanhoList;
}
















