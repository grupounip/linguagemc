#ifndef LISTA_H
#define LISTA_H

template <class List>
class Lista{
	private: 
		List *data;
		int tamanhoList; //tamanho da lista atual
		int capacidadeList; //n�mero de espa�os que a lista cont�m
		void redefinir(); //redefine o tamanho da lista se necess�rio
		bool precisaRedefinir(); // define se � necess�rio redefinir o tamanho da lista
		
	public:
		//construtor
		int listlength;
		int tamanholist;
		Lista(){
			//definir uma lista
			data = new Lista[2];
			//inicializar os parametros
			listlength = 2;
			tamanholist = 0;			
		};
		
	
		
		//acessores
		bool contem(List item);//checa se tem o objeto na lista
		int indexof(List item);//checa aonde est� o objeto selecionado na lista
		int lastindexof(List item);
		List *get(int index); //retorna o obejeto que est� especificado no index
		int tamanho(); //n�mero de objetos preenchidos na lista
		
		//modificadores
		void add(List item);//adiciona um objeto no topo da lista
		void add(int index, List item);//adiciona um objeto em uma posi��o espec�fica
		void set(int index, List item);//muda um objeto em uma posi��o espec�fica
		void remove(int index); //remove um objeto em uma posi��o espec�fica
		
};

#endif
