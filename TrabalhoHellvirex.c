#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

void cadastroRecurso();
void solicitarRecurso(int recursos, int cadastrados);
void cadastroProfessores(int cadastrados);
void imprimirSolicitacao(int solicitacao, int totalRecursos);
bool valida(int id, int contProfessores);
	
	
char nomeRecurso[16][50], caracteristicasRecurso[16][120], nomeProfessor[10][30], emailProfessor[10][50];
int idTipoRecurso[16], matricula[10], qtdRecurso[16];

main(int recursos){
	int opcao, cadastrados, contRecursos;
	char resposta;
	bool repete;
	
	cadastrados = 0; 
	do{
		contRecursos = recursos;
		printf("(1) - Cadastro de recursos \n");
		printf("(2) - Solicitacao de recursos \n");
		printf("(3) - Listar solicitacoes \n");
		printf("(4) - Sair \n");
		printf("Digite uma opcao: \n");
		scanf("%d", &opcao);
		
		switch (opcao) {
			case 1: 
				cadastroRecurso();
				break;
			
			case 2: 
				solicitarRecurso(contRecursos, cadastrados);
				break;
			
			case 3:
				printf("Sistema em desenvolvimento! \n");
				printf("Voce quer cadastrar alguma outra opcao? \n");
				scanf(" %c", &resposta);
				if(resposta == 'S' || resposta =='s'){
					repete = true;
				} else {
				   repete = false;
				}
				break;
			case 4:
				repete = false;
				break;
			
			default: 
				printf("Nao existe essa opcao! \n");
				printf("Quer cadastrar novamente? (S/N) \n");
				scanf(" %c", &resposta);
				if(resposta == 'S' || resposta =='s'){
					repete = true;
				} else {
				   repete = false;
				}		
		}
		
		cadastrados++;
	} while (repete == true && cadastrados < 10);
}

void cadastroRecurso(){
	int totalRecursos;
	char resposta;
	
	printf("--------Cadastre seus recursos-------- \n");
	
	totalRecursos = 0;
	do{
		idTipoRecurso[totalRecursos] = totalRecursos;
		printf("Digite o nome do recurso \n");
		scanf(" %s", &nomeRecurso[totalRecursos]);
		printf("Digite a quantidade de %s disponiveis : \n" , nomeRecurso[totalRecursos]);
		scanf("%d", &qtdRecurso[totalRecursos]);
		printf("Descreva o recurso %s \n", nomeRecurso[totalRecursos]);
		scanf(" %s", &caracteristicasRecurso[totalRecursos]);
		printf("Quer cadastrar mais recursos? (S/N) \n");
		scanf(" %c", &resposta);
		totalRecursos ++;
	} while((resposta == 'S' || resposta =='s') && totalRecursos < 15);	
	
	main(totalRecursos);
}

void solicitarRecurso(int recursos, int cadastrados){
	int i, escolha, totalRecursos, solicitacao;
	cadastroProfessores(cadastrados);
	
	//Listagem de recursos função para alteração solicitaRercuços ta estranho esse +1 -1 kkk
	
	printf("--------Solicitacao de professor %s --------\n", nomeProfessor[i]);
	printf("--------Recursos disponiveis-------- \n");
	for(i=0; i<recursos; i++){
		printf("Recurso %d : %s \n", idTipoRecurso[i] + 1, nomeRecurso[i]);
	}
	totalRecursos = i;
	printf("Escolha o tipo de recurso: \n");
	scanf("%d", &escolha);
	solicitacao = escolha - 1;
	
	imprimirSolicitacao(solicitacao, totalRecursos);
}

void imprimirSolicitacao(int solicitacao, int totalRecursos){
	int i;
	printf("--------Relatorio de solicitacao do professor %s !-------- \n", nomeProfessor[i]);
	for(i=0; i<totalRecursos; i++){
		if (solicitacao == idTipoRecurso[i]){
			printf("Identidade do recurso escolhido e %d e nome %s \n", idTipoRecurso[i] + 1, nomeRecurso[i]);
		}
	}
//	main();
}

void cadastroProfessores(int cadastrados){
	bool validacao;
	int identificador, totalProfessores;
	char resposta;
	
	totalProfessores = cadastrados;
	printf("--------Cadastro de Professores--------\n");
	
	printf("Digite sua matricula: \n");
	do{
		scanf("%d", &identificador);
		validacao = valida(identificador, totalProfessores);
		if (validacao == true){
			printf("Digite sua matricula novamente: \n");
		}
	} while (validacao == true);
	matricula[totalProfessores] = identificador;
	printf("Digite o seu nome: \n");
	scanf(" %s", &nomeProfessor[totalProfessores]);
	printf("Digite o seu e-mail \n");
	scanf(" %s", &emailProfessor[totalProfessores]); 
	
}	
bool valida(int id, int contProfessores){
	int i;
	bool ok;
	ok = false;
		for(i = 0; i < contProfessores; i++){
			if(matricula[i] == id){
				ok = true;
			} else {
				ok = false;
			}
		}
	return ok;
} 
